Instrucciones:

Es necesario tener GIT instalado en su computadora:

Abrir su consola preferida

1) Clonar repositorio - git clone https://CDGL1996@bitbucket.org/CDGL1996/pj-prueba-tecnica.git

2) Instalar las librerias por medio del comando "npm install", estando dentro del repositorio.

3) Escriba "npm start" para correr el servidor.

4) La aplicacion estara funcionando en el puerto 3000 - http://localhost:3000