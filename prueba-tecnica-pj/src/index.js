import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import RootReducer from './reducers/RootReducer';
import ProductsPage from './components/ProductsPage/ProductsPage';
import * as serviceWorker from './serviceWorker';
import thunk from 'redux-thunk';

let initialState = { 
    total: 0.0,
    products: [],
    cart: [],
};

export const store = createStore(RootReducer, initialState, applyMiddleware(thunk));

render(
    <Provider store={store}>
        <ProductsPage />
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
