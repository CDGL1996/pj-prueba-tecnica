import { GET_PRODUCTS, ADD_PRODUCT, DELETE_PRODUCT, LOAD_CART } from './Types';

export const getProducts = (products) => ({
    type: GET_PRODUCTS,
    productList: products.amiibo,
});

export const addProducts = (product) => ({
    type: ADD_PRODUCT,
    product: product
});

export const deleteProducts = (product) => ({
    type: DELETE_PRODUCT,
    product: product
});

export const getCartProducts = (productList) => ({
    type: LOAD_CART,
    productList: productList
});


