import axios from 'axios';
import { getProducts, addProducts, deleteProducts, getCartProducts } from './Actions'; 

export const loadProducts = (characterName) => {

    return (dispatch) => {

        return axios.get('https://www.amiiboapi.com/api/amiibo/?name=' + characterName)
            .then(response => {
                dispatch(getProducts(response.data));
            })
            .catch(function (error) {
                if (error.response.status === 500 || error.response.status === 404) {
                    alert('No se encontro ningun personaje con este nombre!');
                    return false;
                }
            });
    };


  }

  export const addNewProduct = (product) => {

    alert("Producto agregado al carrito.");

    return (dispatch) => {
      dispatch(addProducts(product));
    };

  };

  export const deleteExistingProduct = (product) => {

    alert("Producto eliminado del carrito.");

    return (dispatch) => {
      dispatch(deleteProducts(product));
    };

  };

  export const loadCart = (products) => {

    return (dispatch) => {
      dispatch(getCartProducts(products));
    };

  };
