import { GET_PRODUCTS, ADD_PRODUCT, DELETE_PRODUCT, LOAD_CART} from '../actions/Types';

const randomPrices = (productList) => {

    productList.forEach(product => {
        let randomPrice = parseFloat(1 + Math.random() * (2000 - 1)).toFixed(2);
        product.price = randomPrice;
    });

};

const RootReducer = (state, action) => {
    switch (action.type) {

        case LOAD_CART:
            return {
                ...state,
                cart: action.productList
            };

        case GET_PRODUCTS:

            randomPrices(action.productList);

            return { 
                ...state,
                products: action.productList
             };

        case ADD_PRODUCT:

            return {
                ...state,
                cart: state.cart.concat(action.product),
            };

        case DELETE_PRODUCT:

            if(state.cart.length > 0) {

                let productName = action.product.amiiboSeries.toUpperCase().trim();
                let characterName = action.product.name.toUpperCase().trim();

                let updatedList = [];

                state.cart.forEach(product => {

                    let title = product.amiiboSeries.toUpperCase().trim();
                    let character = product.name.toUpperCase().trim();

                    if(title !== productName || character !== characterName) {
                        updatedList.push(product);
                    }

                });

                localStorage.setItem("shoppingList", JSON.stringify(updatedList));
               
                return {
                    ...state.cart.slice(0, 1),
                    cart: updatedList
                };

            } else {

                return {
                    ...state
                };

            }

        default:
            return state

    }
}

export default RootReducer;