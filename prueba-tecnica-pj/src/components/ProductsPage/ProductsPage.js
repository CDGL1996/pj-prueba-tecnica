import React, { Component } from 'react';
import './ProductsPage.css';
import PapaJohnLogo from '../../static-files/images/PJ-Logo.png'
import ProductsList from '../ProductList/ProductList';
import MiniCart from '../MiniCart/MiniCart';
import CheckOut from '../CheckOut/CheckOut';

import { Route, BrowserRouter as Router } from 'react-router-dom';

class ProductsPage extends Component {

  render() {

    return (

      <Router>
  
      <div id="container">

        <div id="container-header">

          <div id="header-left">
            <img id="papa-john-logo" src={PapaJohnLogo} alt="Load failed"></img>
          </div>

          <div id="header-right">
            <p id="header-title">Prueba Técnica - ReactJS & Redux</p>
          </div>

          <div id="header-bar"></div>

        </div>

        <div id="container-footer">
          
        </div>
  
        <Route exact path="/" component={ProductsList} />
        <Route path="/products" component={ProductsList} />
        <Route path="/shopping-cart" component={MiniCart} />
        <Route path="/check-out" component={CheckOut} />

      </div>
  
      </Router>
  
    );

  }
}

export default ProductsPage;