import React, { Component } from 'react';
import './CheckOut.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class CheckOut extends Component {

openModalWindow = () => {
  document.getElementById("myModal").style.display = "block";
};

closeModalWindow = () => {
  document.getElementById("myModal").style.display = "none";
}

  render() {

    window.onclick = function(event) {
      if (event.target === document.getElementById("myModal")) {
        document.getElementById("myModal").style.display = "none";
      }
    }

    return (
      <div>

        <button id="myBtn" onClick={this.openModalWindow}>Open Modal</button>

        <div id="myModal" className="modal">
          <div className="modal-content">

            <div className="modal-header">
              <h4>Confirmación de Compra</h4>
            </div>

            <div className="modal-body">
              <p>{this.props.testProp}</p>
              <p><b>Esta seguro de que desea comprar estos productos ?</b></p>
              <p>Monto a pagar: {this.props.totalProp} CLP</p>
              <button id="yes-button">Si</button>
              <button id="no-button" onClick={this.closeModalWindow}>No</button>
            </div>

            <div className="modal-footer">
              <h4>Papa Johns - Technical Test</h4>
            </div>
          </div>

        </div>

      </div>
  );

  }

}

export default CheckOut;
