import React from 'react';
import { render } from '@testing-library/react';
import CheckOut from './CheckOut';

test('renders learn react link', () => {
  const { getByText } = render(<CheckOut />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
