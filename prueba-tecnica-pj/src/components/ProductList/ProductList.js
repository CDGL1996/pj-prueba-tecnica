import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import './ProductList.css';
import { connect } from 'react-redux';
import { loadProducts, addNewProduct } from '../../actions/Thunks';
import CartIcon from '../../static-files/images/Cart-Icon.png';


import {
  Link
} from "react-router-dom";

class ProductList extends Component {
   
  updateCharacterName = (event) => {
    this.characterName = event.target.value;
  };

  submitForm = () => {

    let nombre = this.characterName;

    if(nombre === undefined) {
      alert("Debe escribir el nombre de un producto");
      return false;
    }
    if(nombre.length < 3) {
      alert("El nombre debe poseer al menos 3 caracteres");
      return false;
    }
    else {
      this.props.getProducts(this.characterName);
    }

  }

  dispatchAddProduct = (product) => {
     this.props.addProduct(product);
  };

  redirectShoppingCart = () => {

    if(this.props.cart !== undefined && this.props.cart.length > 0) {
      localStorage.setItem("shoppingList", JSON.stringify(this.props.cart));
    }
    
    const cartURL = "/shopping-cart";
    this.props.history.push(cartURL)

  };

  render() {

  const { products } = this.props;

  return (
    
    <div id="product-list-body">

      <div id="product-list-search">

      <form onSubmitCapture={e => { e.preventDefault(); }} onSubmit={this.submitForm}>

        <p>Buscar Producto</p>
        
        <input type="text" onKeyUp= {(event) => this.updateCharacterName(event)} />
        <br />
        <button id="product-search-button">Buscar</button>

      </form>

        <Link onClick={this.redirectShoppingCart} to="/shopping-cart" id="shopping-button">
          <img src={CartIcon} alt="Loading failed" id="shopping-link" />
        </Link>
     
      </div>

      <table id="product-table">
        <thead>
          <tr>
            <th>Producto</th>
            <th>Foto</th>
            <th>Nombre</th>
            <th>Tipo</th>
            <th>Precio</th>
            <th>Acción</th>
          </tr>
        </thead>
        <tbody>

        {
          this.props.products !== undefined ? 
          products.map((product, index) =>
            <tr key={index}>
              <td>
                {product.amiiboSeries}
              </td>
              <td>
                 <img src={product.image} className="product-image" alt="Failed to load" />
              </td>
              <td>
                {product.name}
              </td>
              <td>
                {product.type}
              </td>
              <td>
                {product.price}
              </td>
              <td>
                <button id="add-button" onClick={() => this.dispatchAddProduct(product)}>+</button>
              </td>
            </tr>
          ) : null
        }

        </tbody>
      </table>

    </div>

  );

  }
}

const mapStateToProps = state => ({
  ...state
});

const mapDispatchToProps = (dispatch) => ({
  getProducts: (character) => dispatch(loadProducts(character)),
  addProduct: (product) => dispatch(addNewProduct(product))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductList));