import React, { Component } from 'react';
import { connect } from 'react-redux';
import './MiniCart.css';
import ShoppingIcon from '../../static-files/images/Shopping-Icon.png';
import { withRouter } from 'react-router-dom';
import { loadCart, deleteExistingProduct } from '../../actions/Thunks';
import CheckOut from '../CheckOut/CheckOut';

class MiniCart extends Component {

  componentDidMount = () => {
    this.props.loadState(JSON.parse(localStorage.getItem("shoppingList")));
  };

  componentDidUpdate = () => {
    this.countTotal();
  };

  checkOutPage = () => {
    this.props.history.push("/shopping-cart#openModal")
    document.getElementById("myBtn").click();
  };
  
  returnPage = () => {
    this.props.history.push("/")
  };

  countTotal = () => {

    let counter = 0.0;

    JSON.parse(localStorage.getItem("shoppingList")).forEach(product => {
      counter += parseFloat(product.price);
    });

    return counter;

  };

  dispatchDeleteProduct = (product) => {
    this.props.deleteProduct(product);
  };
  
  render() {

    const { cart } = this.props;

    let total = this.countTotal();
    let cartList = cart;

    return (

      <div>

        <div id="container-header">
          <img id="header-image" src={ShoppingIcon} alt="Loading failed" />
          <h3>Carrito de Compras</h3>
        </div>

        <div id="shopping-body">

        {cartList.map((product, index) =>
            <div className="cart-product" key={index}>
              <table className="cart-table">
                <tbody>
                <tr>
                  <th>
                    {product.amiiboSeries}
                  </th>
                  <th>
                    <img src={product.image} className="product-image" alt="Failed to load" />
                  </th>
                  <th>
                    {product.name}
                  </th>
                  <th>
                    {product.type}
                  </th>
                  <th>
                    {product.price} CLP
                  </th>
                  <th>
                  <button id="remove-button" onClick={() => this.dispatchDeleteProduct(product)}>-</button>
                  </th>
                </tr>
                </tbody>
              </table>
            </div>
          )}

        <p id="total-pay">Total a pagar: {total.toFixed(2)} CLP</p>

        </div>

        <div id="action-buttons">
          <button id="checkout-button" onClick={this.checkOutPage} href="#openModal">Check Out</button>
          <button id="return-button" onClick={this.returnPage}>Atras</button>
        </div>
        

      <CheckOut totalProp = {total.toFixed(2)} />

      </div>

    );
  }
}

const mapStateToProps = state => ({
  ...state
});

const mapDispatchToProps = (dispatch) => ({
  loadState: (productList) => dispatch(loadCart(productList)),
  deleteProduct: (product) => dispatch(deleteExistingProduct(product))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MiniCart));
