import React from 'react';
import { render } from '@testing-library/react';
import MiniCart from './MiniCart';

test('renders learn react link', () => {
  const { getByText } = render(<MiniCart />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
